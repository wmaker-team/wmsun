wmsun (1.06-3) unstable; urgency=medium

  * d/control
    - add myself to `Uploaders`
  * d/patches
    - add patch to remove pre-ANSI function declarations (Closes: #1098109)

 -- Jeremy Sowden <azazel@debian.org>  Thu, 27 Feb 2025 21:14:15 +0000

wmsun (1.06-2) unstable; urgency=medium

  * Team upload.
  * d/control: bump Standards-Version to 4.7.0
  * d/patches: add patch to fix type of arguments to `gmtime_r(3)` and
    `localtime_r(3)` (Closes: #1091242)

 -- Jeremy Sowden <azazel@debian.org>  Tue, 24 Dec 2024 16:48:16 +0000

wmsun (1.06-1) unstable; urgency=medium

  [ Doug Torrance ]
  * New upstream release.
  * debian/control
    - Update Maintainer email address to tracker.d.o.
    - Update Vcs-* after migration to Salsa.
    - Update Homepage to dockapps.net.
    - Drop versioned dependency on libdockapp-dev; was >= 1:0.7.0, but
      1:0.7.2 is now in o-o-stable.
    - Bump Standards-Version to 4.6.0.
    - Add Rules-Requires-Root field (no).
  * debian/copyright
    - Update Format to https.
    - Update upstream mailing list address.
    - Update Source to dockapps.net.
  * debian/menu
    - Remove deprecated Debian menu file.
  * debian/rules
    - Remove out-of-date get-orig-source target.
    - Export all hardening flags.
  * debian/salsa-ci.yml
    - Add Salsa pipeline config file.
  * debian/metadata/upstream
    - Add DEP-12 upstream metadata file.
  * debian/watch
    - Bump to version 4 format.
    - Update download url to dockapps.net.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on debhelper.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Use secure URI in Homepage field.
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.

 -- Doug Torrance <dtorrance@piedmont.edu>  Mon, 30 Aug 2021 12:28:50 -0400

wmsun (1.05-1) unstable; urgency=medium

  * New upstream release.
  * debian/control
    - Update Vcs-* fields.
    - Set Debian Window Maker Team as Maintainer; move myself to
      Uploaders with updated email address.
    - Add libdockapp-dev to Build-Depends.
    - Tidy up using wrap-and-sort.
  * debian/copyright
    - Update email address.
    - Remove wmgeneral files; these have been moved to libdockapp.

 -- Doug Torrance <dtorrance@piedmont.edu>  Fri, 28 Aug 2015 07:57:05 -0400

wmsun (1.04-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #788836).
  * debian/compat
    - Bump to 9.
  * debian/control
    - Bump Build-Depends debhelper version to >= 9.
    - Remove DM-Upload-Allowed field.
    - Bump Standards-Version to 3.9.6.
    - Add Homepage and Vcs-*.
  * debian/copyright
    - Update to DEP5.
  * debian/patches
    - Remove directory; patches applied upstream.
  * debian/{post*,README.Debian}
    - Remove files; rendered obsolete by dh_installmenu and/or the removal of
      debconf in version 1.03-23.
  * debian/rules
    - Add get-orig-source target.
    - Update to use dh.
    - Add override_dh_auto_build target to pass CFLAGS from dpkg-buildflags.
  * debian/watch
    - Add download location.
  * debian/wmsun.install
    - Remove file; no longer necessary.

 -- Doug Torrance <dtorrance@monmouthcollege.edu>  Mon, 15 Jun 2015 08:23:05 -0500

wmsun (1.03+1-2) unstable; urgency=low

  * Bump standards version to 3.8.4.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Add ${misc:Depends} to the binary dependencies.

 -- Denis Briand <denis@narcan.fr>  Sun, 14 Mar 2010 18:02:23 +0100

wmsun (1.03+1-1) unstable; urgency=low

  * Use a clean upstream sources tarball.
  * Use quilt patch system.
  * Bump standards version to 3.8.3.
  * Remove Martin A. Godisch from uploaders field
    Thanks to him to have sponsored wmmoonclock.
  * Allow Debian Maintainers upload.
  * Use debhelper (>= 7).
  * Add compat file.
  * Add watch file.
  * Add README.source file.
  * Fix copyright-refers-to-symlink-license lintian tag.
  * Fix hyphen-used-as-minus-sign lintian tag into man file.
  * Remove debian/changelog.upstream and use patch to add it in the sources
  * Use symlink `wmsun' into debian/menu than `wmSun' binary file name.
  * Add #DEBHELPER# into postinst and postrm files.

 -- Denis Briand <denis@narcan.fr>  Mon, 02 Nov 2009 11:46:30 +0100

wmsun (1.03-29) unstable; urgency=low

  * Add Co-maintainer Martin A. Godisch
  * Update standards version to 3.8.1

 -- Denis Briand <denis@narcan.fr>  Fri, 10 Apr 2009 18:00:50 +0200

wmsun (1.03-28) unstable; urgency=low

  * New maintainer (Closes: #455543).

 -- Denis Briand <denis@narcan.fr>  Sun, 08 Mar 2009 15:24:21 +0100

wmsun (1.03-27) unstable; urgency=low

  * Apply patch for DST changes (Closes: #414489).
    Thanks to Martin Stigge.
  * Remove db_purge in postrm (Closes: #502521).
    Thanks to Cesare Tirabassi.
  * Fix copyright file.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 26 Oct 2008 10:03:16 +0100

wmsun (1.03-26) unstable; urgency=low

  * Enhance display refresh rate (Closes: #446681).
    Thanks to Peter Colberg.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 02 Feb 2008 17:57:36 +0100

wmsun (1.03-25) unstable; urgency=low

  * Fix clean target (Closes: #445622).

 -- Martin A. Godisch <godisch@debian.org>  Sun, 07 Oct 2007 12:41:45 +0200

wmsun (1.03-24) unstable; urgency=low

  * Change display refresh rate to 100 ms, see: #440406.
  * Update menu section.
  * Fix distclean target.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 07 Oct 2007 09:28:40 +0200

wmsun (1.03-23) unstable; urgency=low

  * Remove debconf, add README.Debian (Closes: #413517).
  * Fix timestamps.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 25 Mar 2007 10:40:34 +0200

wmsun (1.03-22) unstable; urgency=low

  * Update Japanese translation file (Closes: #405776).
    Thanks to Hideki Yamane.

 -- Martin A. Godisch <godisch@debian.org>  Wed, 10 Jan 2007 19:26:12 +0100

wmsun (1.03-21) unstable; urgency=low

  * Update control file.
  * Update French translation file (Closes: #402672).
    Thanks to Christian Perrier.
  * Update Czech localization file (Closes: #404382).
    Thanks to Miroslav Kure.

 -- Martin A. Godisch <godisch@debian.org>  Tue, 09 Jan 2007 18:43:20 +0100

wmsun (1.03-20) unstable; urgency=low

  * Fix debconf spelling.
  * Update German debconf translation.
  * Update control file.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 10 Dec 2006 10:04:48 +0100

wmsun (1.03-19) unstable; urgency=low

  * Fix longitude/latitude information URL (Closes: #401872).
    Thanks to Jacopo.
  * Update German debconf translation.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 10 Dec 2006 09:23:55 +0100

wmsun (1.03-18) unstable; urgency=low

  * Update build-dependencies according to xlibs-dev transition.

 -- Martin A. Godisch <godisch@debian.org>  Wed, 30 Nov 2005 19:07:25 +0100

wmsun (1.03-17) unstable; urgency=low

  * Add Vietnamese debconf translation (Closes: #324132).
    Thanks to Clytie Siddall.
  * Add Swedish debconf translation (Closes: #331190).
    Thanks to Daniel Nylander.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Wed, 05 Oct 2005 19:43:35 +0200

wmsun (1.03-16) unstable; urgency=low

  * Add Czech debconf translation (Closes: #294734).
    Thanks to Miroslav Kure.
  * Fix POSIX compliance of debian/rules.
  * Remove watch file, upstream homepage is gone.

 -- Martin A. Godisch <godisch@debian.org>  Fri, 11 Feb 2005 22:18:05 +0100

wmsun (1.03-15) unstable; urgency=low

  * Add Japanese debconf translation (Closes: #236819).
    Thanks to Hideki Yamane.
  * Remove ucf/debconf and lintian workarounds.
  * Update debian/menu, debian/rules, and standards version.

 -- Martin A. Godisch <godisch@debian.org>  Mon, 08 Mar 2004 16:41:45 +0100

wmsun (1.03-14) unstable; urgency=low

  * Simplified ucf handling, see bug #199233, adjusted ucf dependency
    (Closes: #201574).
  * Add Spanish debconf translation (Closes: #201902).
    Thanks to Carlos Alberto Martín Edo.
  * Remove useless BUGS file.
  * Converte debian/changelog to UTF-8.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 27 Jul 2003 15:56:32 +0200

wmsun (1.03-13) unstable; urgency=low

  * Improved ucf transition.

 -- Martin A. Godisch <godisch@debian.org>  Sun, 29 Jun 2003 10:24:00 +0200

wmsun (1.03-12) unstable; urgency=low

  * Update deb'configuration, added ucf support.

 -- Martin A. Godisch <godisch@debian.org>  Fri, 27 Jun 2003 07:11:55 +0200

wmsun (1.03-11) unstable; urgency=low

  * Update deb'configuration.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Fri, 30 May 2003 18:38:35 +0200

wmsun (1.03-10) unstable; urgency=low

  * Add french debconf translation (Closes: #189225).
    Thanks to Christian Perrier.
  * Update configuration.

 -- Martin A. Godisch <godisch@debian.org>  Wed, 16 Apr 2003 09:52:25 +0200

wmsun (1.03-9) unstable; urgency=low

  * Add portuguese debconf translation (Closes: #185684).
    Thanks to André Luís Lopes.
  * Improve debconf template, see #185710.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 22 Mar 2003 07:33:40 +0100

wmsun (1.03-8) unstable; urgency=low

  * Transition to po-debconf, updated build-dependencies.
  * Fix package description.
  * Update standards version.

 -- Martin A. Godisch <godisch@debian.org>  Fri, 14 Mar 2003 08:09:42 +0100

wmsun (1.03-7) unstable; urgency=low

  * Make debian/config asking wmmoonclock for default values.
  * Fix update-menus in debian/postinst and debian/postrm.
  * Add upstream changelog.
  * Add upstream homepage to package description.

 -- Martin A. Godisch <godisch@debian.org>  Sat, 01 Mar 2003 09:54:54 +0100

wmsun (1.03-6) unstable; urgency=low

  * Fix Makefile and compiler warnings.
  * Add Lintian override about menu file not in DEBIAN/md5sums,
    because it is debconf'igured.
  * Simplify debian/config.
  * Remove build-dependency on debhelper.
  * Update maintainer email address.

 -- Martin A. Godisch <godisch@debian.org>  Fri, 21 Feb 2003 20:22:10 +0100

wmsun (1.03-5) unstable; urgency=low

  * Fix handling of menu file (Closes: #161745).
  * Update Debian standards version.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Sat, 21 Sep 2002 15:41:40 +0200

wmsun (1.03-4) unstable; urgency=low

  * Add symbolic links "wmsun -> wmSun" and "wmsun.1.gz -> wmSun.1.gz"
    (Closes: #149923).
  * Add Spanish debconf translation.
    Thanks to Ricardo Javier Cardenes <ricardo@conysis.com>.
  * Add debian/watch.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Fri, 30 Aug 2002 18:49:06 +0200

wmsun (1.03-3) unstable; urgency=low

  * Moved files from /usr/X11R6 to /usr.
  * Update standards version.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Mon,  4 Mar 2002 02:15:09 +0100

wmsun (1.03-2) unstable; urgency=low

  * Fix architecture in debian/control.

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Sun,  3 Mar 2002 07:51:51 +0100

wmsun (1.03-1) unstable; urgency=low

  * Initial release (Closes: #136355).

 -- Martin A. Godisch <godisch@tcs.inf.tu-dresden.de>  Fri,  1 Mar 2002 21:53:26 +0100
